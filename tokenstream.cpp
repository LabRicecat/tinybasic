#include "tokenstream.hpp"

void ts_set(const tokenstream& n_ts) {
    ts = n_ts;
}

void ts_push(const tokenstream& n_ts) {
    ts_saved.push(ts);
    ts = n_ts;
}

void ts_pop() {
    ts = ts_saved.top();
    ts_saved.pop();
}

bool ts_empty() { return ts.empty(); }

void ts_ensure_empty() {
    if(!ts_empty())
        throw internal_exception{"expected end of tokenstream"};
}

void ts_putback(const tokenstream& s) {
    for(int i = s.size()-1; i != -1; --i)
        ts.insert(ts.begin(), s[i]);
}

std::string ts_next() {
    if(ts.empty())
        throw internal_exception{"EOF"};
    auto n = ts.front();
    ts.erase(ts.begin());
    return n;
}

tokenstream streamify(const std::string& source) {
    tokenstream ts = {""};
    bool quote = false;

    for(auto& i : source) {
        if(i == '"') { 
            quote = !quote;
            if(quote)
                if(!ts.back().empty())
                    ts.push_back("");
        }

        if(isspace(i) && i != '\n' && !quote) {
            if(!ts.back().empty())
                ts.push_back("");
        }
        else if((ts_punctuation.contains(i) || i == '\n') && !quote) {
            if(!ts.back().empty())
                ts.push_back(std::string(1, i));
            else 
                ts.back() += i;
            ts.push_back("");
        }
        else {
            ts.back() += i;
        }
    }
    if(ts.back().empty())
        ts.pop_back();

    return ts;
}
