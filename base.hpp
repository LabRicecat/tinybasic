#ifndef BASE_HPP
#define BASE_HPP

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <stack>

struct internal_exception {
    std::string msg;
};

#define unreachable() \
    throw internal_exception{"unreachable"}

struct value;
using function = std::function<value(std::vector<value>)>;

struct object {
    std::string base;
    std::map<std::string,value> members;  
};

struct value {
    enum {
        NUMBER,
        STRING,
        LIST,
        FUNCTION,
        OBJECT,
    } type = NUMBER;

    long double number = 0.0;
    std::string string = "";
    std::vector<value> list;
    function func;
    object obj;

    value() { }
    value(const long double& d): number(d) { type = NUMBER; }
    value(const std::string& s): string(s) { type = STRING; }
    value(const std::vector<value>& l): list(l) { type = LIST; }
    value(const function& f): func(f) { type = FUNCTION; }
    value(const object& o): obj(o) { type = OBJECT; }

    std::string stringify() {
        if(type == NUMBER)
            return std::to_string(number);
        else if(type == STRING)
            return string;
        else if(type == LIST) {
            std::string s;
            for(auto& i : list)
                s += i.stringify() + ",";
            if(!s.empty())
                s.pop_back();
            return "[" + s + "]";
        }
        else if(type == FUNCTION)
            return "\\Function";
        else if(type == OBJECT)
            return "\\Object";
        
        unreachable();
    }

    bool operator==(const value& v) const {
        if(v.type != type) return false;
        switch(v.type) {
            case NUMBER:
                return v.number == number;
            case FUNCTION:
            case OBJECT:
                return false;
            case STRING:
                return v.string == string;
            case LIST:
                if(v.list.size() != list.size())
                    return false;
                for(size_t i = 0; i < list.size(); ++i)
                    if(!(list[i] == v.list[i]))
                        return false;
                return true;
            default:
                unreachable();
        }
    }
};

struct scope {
    int parent = -1;
    std::map<std::string, value> variables;
    std::map<std::string, function> functions;
    std::map<unsigned int, size_t> labels;
};

using builtin_handler = std::function<void()>;
inline std::map<std::string, builtin_handler> builtins;

inline std::vector<scope> scopes = { {} };
inline std::stack<int> current_scopes = {};

struct state {
    int exec = 0;
    value return_value = 0;
    bool returned = false;
} static state;

#endif
