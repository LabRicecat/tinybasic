#include "expression_nodes.hpp"

bool ts_check_index(size_t* size) {
    if(ts_empty()) return false;
    std::vector<std::string> backlog;
    size_t begin_size = size ? *size : 0;
    size_t s = 0;
    std::string t;

    if(!ts_check_s_punc('[')) 
        goto fail;
    backlog.push_back(ts_next());
    if(size) ++*size;

    if(!ts_check_expression(&s))
        goto fail;
    for(size_t i = 0; i < s; ++i)
        backlog.push_back(ts_next());
    if(size) *size += s;

     if(!ts_check_s_punc(']')) 
        goto fail;
    backlog.push_back(ts_next());
    if(size) ++*size;

    ts_putback(backlog);

    return true;

fail:
    ts_putback(backlog);

    if(size) *size = begin_size;
    return false;
}

size_t ts_get_index() {
    if(ts_check_index()) {
        ts_next();

        auto v = ts_get_expression();
        
        if(v.type != value::NUMBER)
            throw internal_exception{"expected number for index"};

        size_t r = (size_t)v.number - 1;

        ts_next();
        return r;
    }
    else 
        throw internal_exception{"expected index"};
}

bool ts_check_line(size_t*);
bool ts_check_body(size_t* size) {
    if(ts_empty()) return false;
    std::vector<std::string> backlog;
    size_t begin_size = size ? *size : 0;
    
    if(!ts_check_keyword("BEGIN")) 
        goto fail;
    if(size) ++*size;
    backlog.push_back(ts_next());

    while(!ts_check_keyword("END")) {
        while(!ts_empty() && ts_check_newline()) {
            backlog.push_back("\n");
            if(size) ++*size;
        }

        size_t s = 0;
        if(ts_empty() || !ts_check_line(&s))
            goto fail;
        for(size_t i = 0; i < s; ++i)
            backlog.push_back(ts_next());
        if(size) *size += s;

        while(!ts_empty() && ts_check_newline()) {
            backlog.push_back("\n");
            if(size) ++*size;
        }
    }
    if(size) ++*size;

    ts_putback(backlog);

    return true;
fail:
    ts_putback(backlog);

    if(size) *size = begin_size;
    return false;
}

std::vector<std::string> ts_get_body() {
    size_t s = 0;
    if(ts_check_body(&s)) {
        std::vector<std::string> r;
        s -= 2;
        
        ts_next();
        for(size_t i = 0; i < s; ++i)
            r.push_back(ts_next());
        ts_next();

        return r;
    }
    else
        throw internal_exception{"expected lambda body"};
}

bool ts_check_lambda(size_t* size) {
    if(ts_empty()) return false;
    std::vector<std::string> backlog;
    size_t begin_size = size ? *size : 0;
    size_t s = 0;

    if(!ts_check_keyword("function"))
        goto fail;
    if(size) ++*size;
    backlog.push_back(ts_next());

    if(!ts_check_paramlist(&s))
        goto fail;
    for(size_t i = 0; i < s; ++i)
        backlog.push_back(ts_next());
    if(size) *size += s;

    s = 0;
    if(!ts_check_body(&s))
        goto fail;
    for(size_t i = 0; i < s; ++i)
        backlog.push_back(ts_next());
    if(size) *size += s;

    ts_putback(backlog);

    return true;

fail:
    ts_putback(backlog);

    if(size) *size = begin_size;
    return false;
}

value eval_function_ts(const std::vector<std::string>&, int, const std::map<std::string, value>&);
function ts_get_lambda() {
    if(ts_check_lambda()) {
        ts_next();
        auto paramlist = ts_get_paramlist();
        auto body = ts_get_body();
        scopes.push_back({.parent = current_scopes.top()});
        int fscope = scopes.size()-1;

        return [paramlist, body, fscope](std::vector<value> args)->value {
            if(paramlist.size() != args.size())
                throw internal_exception{"function argument count does not match"};

            std::map<std::string, value> vars;
            for(size_t i = 0; i < args.size(); ++i)
                vars[paramlist[i]] = args[i];

            return eval_function_ts(body, fscope, vars);
        };
    }
    else 
        throw internal_exception{"expected lambda"};
}

bool ts_check_member(size_t* size) { 
    if(ts_empty()) return false;
    std::vector<std::string> backlog;
    size_t begin_size = size ? *size : 0;
    size_t s = 0;

    if(!ts_check_value(&s)) 
        goto fail;
    for(size_t i = 0; i < s; ++i)
        backlog.push_back(ts_next());
    if(size) *size += s;


    while(ts_check_s_punc('.')) {
        if(!ts_check_identifier())
            goto fail;
        if(size) ++*size;
        backlog.push_back(ts_next());
    }

    ts_putback(backlog);
    return true;
fail:
    ts_putback(backlog);

    if(size) *size = begin_size;
    return false;
}

value ts_get_member() {
    if(ts_check_member()) {
        value v = ts_get_value();
        
        while(ts_check_s_punc('.')) {
            ts_next();
            std::string mem = ts_get_identifier();
            if(v.type != value::OBJECT)
                throw internal_exception{"expected object for member access"};
            if(!v.obj.members.contains(mem)) 
                throw internal_exception{"expected member in object"};

            v = v.obj.members[mem];
        }
        return v;
    }
    else
        throw internal_exception{"expected member access"};
}

bool ts_check_call(size_t* size) {
    if(ts_empty()) return false;
    std::vector<std::string> backlog;
    size_t begin_size = size ? *size : 0;
    size_t s = 0;

    if(!ts_check_member(&s)) 
        goto fail;
    for(size_t i = 0; i < s; ++i)
        backlog.push_back(ts_next());
    if(size) *size += s;

    s = 0;
    while(ts_check_arglist(&s) || ts_check_index(&s)) {
        if(size) *size += s;
        backlog.push_back(ts_next());
    }

    ts_putback(backlog);

    return true;

fail:
    ts_putback(backlog);

    if(size) *size = begin_size;
    return false;
}

value ts_get_call() {
    if(ts_check_call()) {
        value v = ts_get_member();
        while(ts_check_arglist() || ts_check_index()) {
            if(ts_check_arglist()) {
                if(v.type != value::FUNCTION)
                    throw internal_exception{"expected function for call"};

                auto arglist = ts_get_arglist();
                v = v.func(arglist);
            }
            else if(ts_check_index()) {
                if(v.type != value::LIST)
                    throw internal_exception{"expected array for index"};

                size_t idx = ts_get_index();
                if(idx > v.list.size())
                    throw internal_exception{"expected big enough array for index"};
                v = v.list[idx];
            }
            else 
                unreachable();
        } 
        return v;
    }
    else 
        throw internal_exception{"expected call"};
}

bool ts_check_unary(size_t* size) { 
    if(ts_empty()) return false;
    std::vector<std::string> backlog;
    size_t begin_size = size ? *size : 0;
    size_t s = 0;

    while(ts_check_s_punc('-') || ts_check_s_punc('+') || ts_check_s_punc('!')) {
        backlog.push_back(ts_next());
        if(size) ++*size;
    }
    
    if(!ts_check_call(&s)) 
        goto fail;

    if(size) *size += s;

    ts_putback(backlog);

    return true;

fail:
    ts_putback(backlog);

    if(size) *size = begin_size;
    return false;
}

value ts_get_unary() {
    if(ts_check_unary()) {
        std::string opts;
        while(ts_check_s_punc('-') || ts_check_s_punc('+') || ts_check_s_punc('!'))
            opts += ts_next();
        
        value v = ts_get_call();
        if(opts.empty())
            return v;

        if(v.type != value::NUMBER)
            throw internal_exception{"expected number for unary operation"};

        auto num = v.number;
        for(auto& i : opts) {
            switch(i) {
                default: 
                    unreachable();
                case '-':
                    num = -num;
                    break;
                case '+':
                    num = num;
                    break;
                case '!':
                    num = !num;
                    break;
            }
        }

        return num;
    }
    else 
        throw internal_exception{"expected unary"};
}

bool ts_check_term(size_t* size) { 
    if(ts_empty()) return false;
    std::vector<std::string> backlog;
    size_t begin_size = size ? *size : 0;
    size_t s = 0;
    bool proceed = false;
    
    if(!ts_check_unary(&s))
        goto fail;

    do {
        for(size_t i = 0; i < s; ++i)
            backlog.push_back(ts_next());
        if(size) *size += s;

        if(
            !ts_check_s_punc('*') &&
            !ts_check_s_punc('/') &&
            !ts_check_s_punc('%')
        ) { proceed = true; break; }

        backlog.push_back(ts_next());
        if(size) ++*size;
        s = 0;
    } while(ts_check_unary(&s));

    if(!proceed) // operator at the end 
        goto fail;

    ts_putback(backlog);

    return true;
fail:
    ts_putback(backlog);

    if(size) *size = begin_size;
    return false;
}

value ts_get_term() {
    if(ts_check_term()) {
        value lhs = ts_get_unary();

        while(ts_check_s_punc('*') || ts_check_s_punc('/') || ts_check_s_punc('%')) {
            char op = ts_next()[0];
            value rhs = ts_get_unary();

            if(lhs.type != rhs.type && lhs.type != value::NUMBER)
                throw internal_exception{"expected 2 numbers for term"};

            switch(op) {
                case '*':
                    lhs.number = lhs.number * rhs.number;
                    break;
                case '/':
                    lhs.number = lhs.number / rhs.number; // TODO: null division check
                    break;
                case '%':
                    lhs.number = (long)lhs.number % (long)rhs.number;
                    break;
                default:
                    unreachable();
            }
        }

        return lhs;
    }
    else 
        throw internal_exception{"expected term"};
}


bool ts_check_sum(size_t* size) { 
    if(ts_empty()) return false;
    std::vector<std::string> backlog;
    size_t begin_size = size ? *size : 0;
    size_t s = 0;
    bool proceed = false;
    
    if(!ts_check_term(&s))
        goto fail;

    do {
        for(size_t i = 0; i < s; ++i)
            backlog.push_back(ts_next());
        if(size) *size += s;

        if(
            !ts_check_s_punc('-') &&
            !ts_check_s_punc('+')
        ) { proceed = true; break; }

        backlog.push_back(ts_next());
        if(size) ++*size;
        s = 0;
    } while(ts_check_term(&s));

    if(!proceed) // operator at the end 
        goto fail;

    ts_putback(backlog);

    return true;
fail:
    ts_putback(backlog);

    if(size) *size = begin_size;
    return false;
}

value ts_get_sum() {
    if(ts_check_term()) {
        value lhs = ts_get_term();

        while(ts_check_s_punc('-') || ts_check_s_punc('+')) {
            char op = ts_next()[0];
            value rhs = ts_get_term();

            if(lhs.type != rhs.type)
                throw internal_exception{"expected same types in sum"};
            
            switch(op) {
                case '+':
                    if(lhs.type == value::NUMBER)
                        lhs.number = lhs.number + rhs.number;
                    else if(lhs.type == value::STRING)
                        lhs.string = lhs.string + rhs.string;
                    else if(lhs.type == value::LIST) {
                        for(auto& i : rhs.list)
                            lhs.list.push_back(i);
                    }
                    else 
                        throw internal_exception{"expected number|string|array for sum"};
                    break;
                case '-':
                    if(lhs.type != value::NUMBER)
                        throw internal_exception{"expected 2 numbers for sum"};

                    lhs.number = lhs.number - rhs.number;
                    break;
                default:
                    unreachable();
            }
        }

        return lhs;
    }
    else 
        throw internal_exception{"expected sum"};
}

bool ts_check_comparison(size_t* size) { 
    if(ts_empty()) return false;
    std::vector<std::string> backlog;
    size_t begin_size = size ? *size : 0;
    size_t s = 0;
    bool proceed = false;
    
    if(!ts_check_sum(&s))
        goto fail;

    do {
        for(size_t i = 0; i < s; ++i)
            backlog.push_back(ts_next());
        if(size) *size += s;

        if(
            !ts_check_s_punc('=') &&
            !ts_check_s_punc('~') &&
            !ts_check_s_punc('>') &&
            !ts_check_s_punc('<')
        ) { proceed = true; break; }

        backlog.push_back(ts_next());
        if(size) ++*size;
        s = 0;
    } while(ts_check_sum(&s));

    if(!proceed) // operator at the end 
        goto fail;

    ts_putback(backlog);

    return true;
fail:
    ts_putback(backlog);

    if(size) *size = begin_size;
    return false;
}

value ts_get_comparison() {
    if(ts_check_sum()) {
        value lhs = ts_get_sum();

        while(ts_check_s_punc('=') || ts_check_s_punc('~') || ts_check_s_punc('>') || ts_check_s_punc('<')) {
            char op = ts_next()[0];
            value rhs = ts_get_sum();

            if(lhs.type != rhs.type)
                throw internal_exception{"expected same types in comparison"};
            
            switch(op) {
                case '=':
                    lhs.number = lhs == rhs;
                    lhs.type = value::NUMBER;
                    break;
                case '~':
                    lhs.number = lhs != rhs;
                    lhs.type = value::NUMBER;
                    break;
                case '>':
                    if(lhs.type != value::NUMBER)
                        throw internal_exception{"expected number for > comparison"};

                    lhs.number = lhs.number > rhs.number;
                    break;
                case '<':
                    if(lhs.type != value::NUMBER)
                        throw internal_exception{"expected number for < comparison"};

                    lhs.number = lhs.number < rhs.number;
                    break;
                default:
                    unreachable();
            }
        }

        return lhs;
    }
    else 
        throw internal_exception{"expected comparison"};
}

bool ts_check_expression(size_t* size) {
    if(ts_empty()) return false;
    return ts_check_comparison(size);
}

value ts_get_expression() {
    return ts_get_comparison();
}

bool ts_check_variable() {
    if(ts_empty()) return false;
    auto n = ts.front(); 
    return has_var(n);
}

value& ts_get_variable() {
    if(ts_check_variable())
        return get_var(ts_next());
    else 
        throw internal_exception{"expected variable"};
}

bool ts_check_value(size_t* size) {
    if(ts_empty()) return false;
    if(
        ts_check_number() ||
        ts_check_string() ||
        ts_check_variable()
    ) {
        if(size) ++*size;
        return true;
    }
    else if(
        ts_check_list(size) ||
        ts_check_lambda(size)
    ) return true;

    return false;
}

value ts_get_value() {
    if(ts_check_value()) {
        if(ts_check_number())
            return ts_get_number();
        if(ts_check_string())
            return ts_get_string();
        if(ts_check_variable())
            return ts_get_variable();
        if(ts_check_function())
            return ts_get_function();
        if(ts_check_lambda())
            return ts_get_lambda();
        if(ts_check_list())
            return ts_get_list();

        unreachable();
    }
    else 
        throw internal_exception{"expected value"};
}

