#include "statement_nodes.hpp"
#include <iostream>

#define checkfor(name) \
    do { if(!name(&s)) \
        goto fail; \
    for(size_t i = 0; i < s; ++i) \
        backlog.push_back(ts_next()); \
    if(size) *size += s; } while(0)

#define base() \
    if(ts_empty()) return false; \
    std::vector<std::string> backlog; \
    size_t begin_size = size ? *size : 0; \
    size_t s = 0

#define endfunc() \
    fail: \
    ts_putback(backlog); \
    if(size) *size = begin_size; \
    return false; \

#define getcheck(name) \
    if(!ts_check_##name()) \
        throw internal_exception{"expected " # name };

bool ts_check_print(size_t* size) {
    base();    

    checkfor(ts_check_expression);

    endfunc();
}

void ts_get_print() {
    getcheck(print);

    std::cout << ts_get_expression().stringify() << "\n";
}

bool ts_check_builtin() {
    if(ts_empty()) return false;
    auto next = ts.front();
    if(!builtins.contains(next))
        return false;

    return true;
}

std::string ts_get_builtin() {
    if(ts_check_builtin())
        return ts_next();
    else
        throw internal_exception{"expected builtin"};
}

bool ts_check_line(size_t* size) {
    if(ts_empty()) return false;
    std::vector<std::string> backlog;
    size_t begin_size = size ? *size : 0;
    size_t s = 0;

    if(ts_check_integer()) {
        if(size) ++*size;
        backlog.push_back(ts_next());
    }

    if(!ts_check_builtin())
        goto fail;
    if(size) ++*size;
    backlog.push_back(ts_next());

    if(ts_empty() || ts_check_newline() || ts_check_keyword("END")) {
        if(ts_check_newline())
            if(size) ++*size;

        ts_putback(backlog);
        return true;
    }

    while(!ts_empty()) {
        if(s == 0 && ts_check_identifier()) ++s; // TODO: move this to ts_check_expression

        for(size_t i = 0; i < s; ++i)
            backlog.push_back(ts_next());
        if(size) *size += s;
        
        if(ts_empty() || ts_check_newline() || ts_check_keyword("END")) {
            if(ts_check_newline()) 
                if(size) ++*size;
            break;
        }

        s = 1; // TODO: add the missing tokens got from `s` 
    }

    ts_putback(backlog);

    return true;
fail:
    ts_putback(backlog);

    if(size) *size = begin_size;
    return false;
}

line ts_get_line() {
    size_t s = 0;
    if(ts_check_line(&s)) {
        line ln;
        if(ts_check_integer())
            ln.line_count = ts_get_integer(), --s;
        ln.builtin = ts_get_builtin(), --s;

        for(size_t i = 0; i < s-1; ++i)
            ln.args_ts.push_back(ts_next());

        if(ts_check_newline())
            ts_next(); // \n
        else ln.args_ts.push_back(ts_next());

        return ln;
    }
    else 
        throw internal_exception{"expected line"};
}

void eval_line(const line& l) {
    auto builtin = builtins[l.builtin];
    ts_push(l.args_ts);
    builtin();
    ts_pop();
}

void eval_lines(const tokenstream& stream) {
    ts_push(stream);
    while(!ts_empty()) {
        ts_ignore_newlines();
        if(!ts_check_line())
            throw internal_exception{"expected line"};
        auto line = ts_get_line();
        eval_line(line);
        ts_ignore_newlines();
    }
    ts_pop();
}

value eval_function_ts(const std::vector<std::string>& body, int fscope, const std::map<std::string, value>& vars) {
    load_scope(fscope);
    get_scope().variables = vars;
    
    eval_lines(body); 
    if(state.returned) {
        state.returned = false;
        return state.return_value;
    }

    return 0;
}

