#ifndef EXPRESSION_NODES_HPP
#define EXPRESSION_NODES_HPP

#include "base_nodes.hpp"

bool ts_check_index(size_t* size = nullptr);
size_t ts_get_index();

bool ts_check_body(size_t* size = nullptr);
std::vector<std::string> ts_get_body();

bool ts_check_lambda(size_t* size = nullptr);
function ts_get_lambda();

bool ts_check_member(size_t* size = nullptr);
value ts_get_member();

bool ts_check_call(size_t* size = nullptr);
value ts_get_call();

bool ts_check_unary(size_t* size = nullptr);
value ts_get_unary();

bool ts_check_term(size_t* size = nullptr);
value ts_get_term();

bool ts_check_sum(size_t* size = nullptr);
value ts_get_sum();

bool ts_check_comparison(size_t* size = nullptr);
value ts_get_comparison();

bool ts_check_expression(size_t* size = nullptr);
value ts_get_expression();

bool ts_check_variable();
value& ts_get_variable();

bool ts_check_value(size_t* size = nullptr);
value ts_get_value();

#endif
