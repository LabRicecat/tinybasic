PROJ=tinybasic

CXXFLAGS=-std=c++20

all:
	$(CXX) $(CXXFLAGS) $(wildcard *.cpp) -o $(PROJ)

