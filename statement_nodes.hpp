#ifndef STATEMENT_NODES_HPP
#define STATEMENT_NODES_HPP

#include "base.hpp"
#include "expression_nodes.hpp"
#include "scoping.hpp"

bool ts_check_print(size_t* size = nullptr);
void ts_get_print();

bool ts_check_let(size_t* size = nullptr);
void ts_get_let();

bool ts_check_if(size_t* size = nullptr);
void ts_get_if();

bool ts_check_power(size_t* size = nullptr);
void ts_get_power();

bool ts_check_end(size_t* size = nullptr);
void ts_get_end();

bool ts_check_filter(size_t* size = nullptr);
void ts_get_filter();

bool ts_check_reverse(size_t* size = nullptr);
void ts_get_reverse();

bool ts_check_swap(size_t* size = nullptr);
void ts_get_swap();

bool ts_check_printrange(size_t* size = nullptr);
void ts_get_printrange();

bool ts_check_shuffle(size_t* size = nullptr);
void ts_get_shuffle();

bool ts_check_clearall(size_t* size = nullptr);
void ts_get_clearall();

bool ts_check_repeat(size_t* size = nullptr);
void ts_get_repeat();

bool ts_check_fibonacci(size_t* size = nullptr);
void ts_get_fibonacci();

bool ts_check_min(size_t* size = nullptr);
void ts_get_max();

bool ts_check_reversematrix(size_t* size = nullptr);
void ts_get_reversematrix();

bool ts_check_round(size_t* size = nullptr);
void ts_get_round();

bool ts_check_sort(size_t* size = nullptr);
void ts_get_sort();

bool ts_check_slice(size_t* size = nullptr);
void ts_get_slice();

bool ts_check_replace(size_t* size = nullptr);
void ts_get_replace();

bool ts_check_stats(size_t* size = nullptr);
void ts_get_stats();

bool ts_check_matrixmult(size_t* size = nullptr);
void ts_get_matrixmult();

bool ts_check_find(size_t* size = nullptr);
void ts_get_find();

bool ts_check_swaparray(size_t* size = nullptr);
void ts_get_swaparray();

bool ts_check_union(size_t* size = nullptr);
void ts_get_union();

bool ts_check_diff(size_t* size = nullptr);
void ts_get_diff();

bool ts_check_random(size_t* size = nullptr);
void ts_get_random();

bool ts_check_prime(size_t* size = nullptr);
void ts_get_prime();

bool ts_check_timer(size_t* size = nullptr);
void ts_get_timer();

bool ts_check_for(size_t* size = nullptr);
void ts_get_for();

bool ts_check_factorial(size_t* size = nullptr);
void ts_get_factorial();

bool ts_check_clear(size_t* size = nullptr);
void ts_get_clear();

bool ts_check_date(size_t* size = nullptr);
void ts_get_date();

bool ts_check_fib(size_t* size = nullptr);
void ts_get_fib();

bool ts_check_sumstm(size_t* size = nullptr);
void ts_get_sumstm();

bool ts_check_transpose(size_t* size = nullptr);
void ts_get_transpose();

bool ts_check_matrix(size_t* size = nullptr);
void ts_get_matrix();

bool ts_check_count(size_t* size = nullptr);
void ts_get_count();

bool ts_check_printbin(size_t* size = nullptr);
void ts_get_printbin();

bool ts_check_wait(size_t* size = nullptr);
void ts_get_wait();

bool ts_check_play(size_t* size = nullptr);
void ts_get_play();

bool ts_check_draw(size_t* size = nullptr);
void ts_get_draw();

bool ts_check_clearscreen(size_t* size = nullptr);
void ts_get_clearscreen();

bool ts_check_or(size_t* size = nullptr);
void ts_get_or();

bool ts_check_and(size_t* size = nullptr);
void ts_get_and();

bool ts_check_between(size_t* size = nullptr);
void ts_get_between();

bool ts_check_open(size_t* size = nullptr);
void ts_get_open();

bool ts_check_write(size_t* size = nullptr);
void ts_get_write();

bool ts_check_close(size_t* size = nullptr);
void ts_get_close();

bool ts_check_read(size_t* size = nullptr);
void ts_get_read();

bool ts_check_proc(size_t* size = nullptr);
void ts_get_proc();

bool ts_check_try(size_t* size = nullptr);
void ts_get_try();

bool ts_check_throw(size_t* size = nullptr);
void ts_get_throw();

bool ts_check_finally(size_t* size = nullptr);
void ts_get_finally();

bool ts_check_catch(size_t* size = nullptr);
void ts_get_catch();

bool ts_check_endtry(size_t* size = nullptr);
void ts_get_endtry();

bool ts_check_rand(size_t* size = nullptr);
void ts_get_rand();

bool ts_check_split(size_t* size = nullptr);
void ts_get_split();

bool ts_check_until(size_t* size = nullptr);
void ts_get_until();

bool ts_check_type(size_t* size = nullptr);
void ts_get_type();

bool ts_check_new(size_t* size = nullptr);
void ts_get_new();

bool ts_check_redirectin(size_t* size = nullptr);
void ts_get_redirectin();

bool ts_check_redirectout(size_t* size = nullptr);
void ts_get_redirectout();

bool ts_check_def(size_t* size = nullptr);
void ts_get_def();

bool ts_check_gosub(size_t* size = nullptr);
void ts_get_gosub();

bool ts_check_goto(size_t* size = nullptr);
void ts_get_goto();

bool ts_check_clrerr(size_t* size = nullptr);
void ts_get_clrerr();

bool ts_check_callstm(size_t* size = nullptr);
void ts_get_callstm();

bool ts_check_case(size_t* size = nullptr);
void ts_get_case();

bool ts_check_input(size_t* size = nullptr);
void ts_get_input();

bool ts_check_dim(size_t* size = nullptr);
void ts_get_dim();

bool ts_check_while(size_t* size = nullptr);
void ts_get_while();

bool ts_check_break(size_t* size = nullptr);
void ts_get_break();

bool ts_check_endwhile(size_t* size = nullptr);
void ts_get_endwhile();

bool ts_check_switch(size_t* size = nullptr);
void ts_get_switch();

struct line {
    int line_count = 0;
    std::string builtin;
    std::vector<std::string> args_ts;
};

bool ts_check_builtin();

std::string ts_get_builtin();
bool ts_check_line(size_t* size = nullptr);

line ts_get_line();

void eval_line(const line& l);

void eval_lines(const tokenstream& stream);

value eval_function_ts(const std::vector<std::string>& body, int fscope, const std::map<std::string, value>& vars);

#endif
