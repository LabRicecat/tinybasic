#ifndef SCOPING_HPP
#define SCOPING_HPP

#include "base.hpp"

scope& get_scope();

int push_scope(int parent = -2);

void load_scope(int idx);

void pop_scope();

bool has_var(const std::string& var);

bool has_function(const std::string& func);

value& get_var(const std::string& var);

function& get_function(const std::string& func);

#endif
