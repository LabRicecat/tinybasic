#ifndef BASE_NODES_HPP
#define BASE_NODES_HPP

#include <cctype>

#include "base.hpp"
#include "tokenstream.hpp"
#include "scoping.hpp"

bool ts_check_identifier();
std::string ts_get_identifier();

bool ts_check_keyword(const std::string& kw);
std::string ts_get_keyword(const std::string& kw);

bool ts_check_number();
long double ts_get_number();

bool ts_check_integer();
int ts_get_integer();

bool ts_check_string();
std::string ts_get_string();

bool ts_check_newline();
void ts_get_newline();
void ts_ignore_newlines();

bool ts_check_punc();
char ts_get_punc();

bool ts_check_s_punc(char c);
char ts_get_s_punc(char c);

bool ts_check_function();
function& ts_get_function();

bool ts_check_paramlist(size_t* size = nullptr);
std::vector<std::string> ts_get_paramlist();

bool ts_check_arglist(size_t* size = nullptr);
std::vector<value> ts_get_arglist();

bool ts_check_list(size_t* size = nullptr);
std::vector<value> ts_get_list();

#endif
