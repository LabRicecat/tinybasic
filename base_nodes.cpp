#include "base_nodes.hpp"

bool ts_check_identifier() {
    if(ts_empty()) return false;
    auto next = ts.front();
    if(!isalpha(next[0]) && next[0] != '_' && next[0] != '$') 
        return false;
    next.pop_back();
    for(auto& i : next)
        if(!isalnum(next[0]) && next[0] != '_' && next[0] != '$')
            return false;

    return true;
}

std::string ts_get_identifier() {
    if(ts_check_identifier())
        return ts_next();
    else 
        throw internal_exception{"expected identifier"};
}

bool ts_check_keyword(const std::string& kw) {
    if(ts_empty()) return false;
    return ts.front() == kw;
}

std::string ts_get_keyword(const std::string& kw) {
    if(ts_check_keyword(kw))
        return ts_next();
    else 
        throw internal_exception{"expected keyword " + kw};
}

bool ts_check_number() {
    if(ts_empty()) return false;
    bool dot = false;
    auto next = ts.front();
    for(auto& i : next)
        if(!isdigit(i)) {
            if(i == '.' && !dot) dot = true;
            else return false;
        }

    return true;
}

long double ts_get_number() {
    if(ts_check_number())
        return std::stold(ts_next());
    else
        throw internal_exception{"expected number"};
}

bool ts_check_integer() {
    if(ts_empty()) return false;
    auto next = ts.front();
    for(auto& i : next)
        if(!isdigit(i))
            return false;

    return true;
}

int ts_get_integer() {
    if(ts_check_number())
        return std::stoi(ts_next());
    else
        throw internal_exception{"expected integer"};
}

bool ts_check_string() {
    if(ts_empty()) return false;
    auto next = ts.front();
    if(next.size() < 2 || next.front() != '"' || next.back() != '"')
        return false;

    for(size_t i = 1; i < next.size()-1; ++i) 
        if(next[i] == '"' || next[i] == '\n')
            return false;

    return true;
}

std::string ts_get_string() {
    if(ts_check_string()) {
        auto n = ts_next();
        n.pop_back();
        n.erase(n.begin());
        return n;
    }
    else
        throw internal_exception{"expected number"};
}

bool ts_check_newline() {
    if(ts_empty()) return false;
    auto n = ts.front();
    return n == "\n";
}

void ts_get_newline() {
    if(!ts_check_newline())
        throw internal_exception{"expected newline"};
}

void ts_ignore_newlines() {
    while(!ts_empty() && ts_check_newline())
        ts_next();
}

bool ts_check_punc() {
    if(ts_empty()) return false;
    auto n = ts.front();
    if(n.size() != 1 || ts_punctuation.contains(n[0]))
        return false;

    return true;
}

char ts_get_punc() {
    if(ts_check_number())
        return ts_next().front();
    else
        throw internal_exception{"expected punctuation"};
}

bool ts_check_s_punc(char c) {
    if(ts_empty()) return false;
    auto n = ts.front();
    if(n.size() != 1 || n[0] != c)
        return false;

    return true;
}

char ts_get_s_punc(char c) {
    if(ts_check_s_punc(c))
        return ts_next().front();
    else
        throw internal_exception{"expected punctuation " + std::string(1,c)};
}

bool ts_check_function() {
    if(ts_empty()) return false;
    return has_function(ts.front());
}

function& ts_get_function() {
    if(ts_check_function())
        return get_function(ts_next());
    else
        throw internal_exception{"expected function"};
}

bool ts_check_paramlist(size_t* size) {
    if(ts_empty()) return false;
    std::vector<std::string> backlog;
    size_t begin_size = size ? *size : 0;
    std::string t;

    if(!ts_check_s_punc('(')) 
        goto fail;
    backlog.push_back(ts_next());
    if(size) ++*size;

    while(!ts_empty()) {
        if(ts_check_s_punc(')'))
            break;

        if(!ts_check_identifier())
            goto fail;
        backlog.push_back(ts_next());
        if(size) ++*size;

        if(!ts_check_s_punc(',')) {
            if(ts_check_s_punc(')'))
                break;
            else 
                goto fail;
        }
        backlog.push_back(ts_next());
        if(size) ++*size;
    }

     if(!ts_check_s_punc(')')) 
        goto fail;
    backlog.push_back(ts_next());
    if(size) ++*size;

    ts_putback(backlog);

    return true;

fail:
    ts_putback(backlog);

    if(size) *size = begin_size;
    return false;
}

std::vector<std::string> ts_get_paramlist() {
    if(ts_check_paramlist()) {
        ts_next();
        std::vector<std::string> r;
        while(!ts_check_s_punc(')')) {
            r.push_back(ts_next());
            if(ts_check_s_punc(','))
                ts_next();
        }
        ts_next();

        return r;
    }
    else 
        throw internal_exception{"expected parameter list"};
}

bool ts_check_expression(size_t*);
value ts_get_expression();

bool ts_check_arglist(size_t* size) {
    if(ts_empty()) return false;
    std::vector<std::string> backlog;
    size_t begin_size = size ? *size : 0;
    size_t s = 0;
    std::string t;

    if(!ts_check_s_punc('(')) 
        goto fail;
    backlog.push_back(ts_next());
    if(size) ++*size;

    while(!ts_empty()) {
        s = 0;
        if(!ts_check_expression(&s))
            goto fail;
        for(size_t i = 0; i < s; ++i)
            backlog.push_back(ts_next());
        if(size) *size += s;

        if(!ts_check_s_punc(',')) {
            if(ts_check_s_punc(')'))
                break;
            else 
                goto fail;
        }
        backlog.push_back(ts_next());
        if(size) ++*size;
    }

     if(!ts_check_s_punc(')')) 
        goto fail;
    backlog.push_back(ts_next());
    if(size) ++*size;

    ts_putback(backlog);

    return true;

fail:
    ts_putback(backlog);

    if(size) *size = begin_size;
    return false;
}

std::vector<value> ts_get_arglist() {
    if(ts_check_arglist()) {
        ts_next();
        
        std::vector<value> r;
        while(!ts_check_s_punc(')')) {
            r.push_back(ts_get_expression());
            if(ts_check_s_punc(','))
                ts_next();
        }

        ts_next();
        return r;
    }
    else 
        throw internal_exception{"expected argument list"};
}

bool ts_check_list(size_t* size) {
    if(ts_empty()) return false;
    std::vector<std::string> backlog;
    size_t begin_size = size ? *size : 0;
    size_t s = 0;
    std::string t;

    if(!ts_check_s_punc('[')) 
        goto fail;
    backlog.push_back(ts_next());
    if(size) ++*size;

    while(!ts_empty()) {
        if(ts_check_s_punc(']')) {
            break;
        }
        s = 0;
        if(!ts_check_expression(&s))
            goto fail;
        for(size_t i = 0; i < s; ++i)
            backlog.push_back(ts_next());
        if(size) *size += s;

        if(!ts_check_s_punc(',')) {
            if(ts_check_s_punc(']'))
                break;
            else 
                goto fail;
        }
        backlog.push_back(ts_next());
        if(size) ++*size;
    }

     if(!ts_check_s_punc(']')) 
        goto fail;
    backlog.push_back(ts_next());
    if(size) ++*size;

    ts_putback(backlog);

    return true;

fail:
    ts_putback(backlog);

    if(size) *size = begin_size;
    return false;
}

std::vector<value> ts_get_list() {
    if(ts_check_list()) {
        ts_next();
        
        std::vector<value> r;
        while(!ts_check_s_punc(']')) {
            r.push_back(ts_get_expression());
            if(!ts_check_s_punc(',')) 
                break;
            ts_next();
        }

        ts_next();
        return r;
    }
    else 
        throw internal_exception{"expected list"};
}

