#include "scoping.hpp"

scope& get_scope() {
    return scopes[current_scopes.top()];
}

int push_scope(int parent) {
    scope s;
    s.parent = parent == -2 ? current_scopes.top() : parent;
    scopes.push_back(s);
    current_scopes.push(scopes.size() - 1);
    return scopes.size() - 1;
}

void load_scope(int idx) {
    current_scopes.push(idx);
    get_scope().variables.clear();
    get_scope().functions.clear();
}

void pop_scope() {
    current_scopes.pop();
}

bool has_var(const std::string& var) {
    int c = current_scopes.top();
    do {
        if(scopes[c].variables.contains(var))
            return true;
        c = scopes[c].parent;
    } while(c != -1);

    return false;
}

bool has_function(const std::string& func) {
    int c = current_scopes.top();
    do {
        if(scopes[c].functions.contains(func))
            return true;
        c = scopes[c].parent;
    } while(c != -1);

    return false;
}

value& get_var(const std::string& var) {
    int c = current_scopes.top();
    do {
        if(scopes[c].variables.contains(var))
            return scopes[c].variables[var];
        c = scopes[c].parent;
    } while(c != -1);

    throw internal_exception{""};
}

function& get_function(const std::string& func) {
    int c = current_scopes.top();
    do {
        if(scopes[c].functions.contains(func))
            return scopes[c].functions[func];
        c = scopes[c].parent;
    } while(c != -1);

    throw internal_exception{""};
}

