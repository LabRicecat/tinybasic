#ifndef TOKENSTREAM_HPP
#define TOKENSTREAM_HPP

#include <set>
#include <stack>
#include "base.hpp"

using tokenstream = std::vector<std::string>;
inline tokenstream ts;

inline std::stack<tokenstream> ts_saved;

void ts_set(const tokenstream& n_ts);

void ts_push(const tokenstream& n_ts);

void ts_pop();

const inline std::set<char> ts_punctuation = {
    '+', '-', '*', '=', '~', '/', ',',
    '!', '{', '}', '(', ')', '[', ']',
    '>', '<', '|', ';', '.', ':', '#',
    '%', '&', '\\', '?'
};

bool ts_empty();

void ts_ensure_empty();

void ts_putback(const tokenstream& s);

std::string ts_next();

tokenstream streamify(const std::string& source);

#endif
