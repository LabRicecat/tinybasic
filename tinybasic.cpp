#include <fstream>
#include <iostream>
#include "statement_nodes.hpp"

void init() {
    current_scopes.push(0);
}

std::string rfile(const std::string& file) {
    std::ifstream f(file);
    std::string r;

    int c;
    while((c = f.get()) != EOF)
        r += c;
    f.close();

    return r;
}

int main(int argc, char** argv) {
    init();
    
    tokenstream s = streamify(rfile(std::string(argv[1])));

    try {
        eval_lines(s);
    }
    catch(internal_exception& err) {
        std::cerr << "[ERROR]: " << err.msg << "\n";
    }
}
